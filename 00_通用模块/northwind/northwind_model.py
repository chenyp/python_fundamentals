#!/usr/bin/env python
# -*- coding:UTF-8 -*-
"""
dongxiaoyong
2018/4/23
northwind_model.py

ProductId: 产品编号
ProductName: 产品名称
SupplierId: 供应商编号
SupplierName: 供应商名称
CategoryId: 类别编号
CategoryName: 类别名称
UnitNum: 规格描述
UnitPrice: 产品单价
StockNum: 库存量
OrderNum: 订购量
ReOrderNum: 续订量
IsAbord: 是否终止供应, True 为终止供应, False 为正常供应

northwind_model.py 文件用于定义产品类, 包含 json 文件中的所有产品属性
"""
import json


class Product(object):
    """产品类, 根据 json 数据定义产品类型"""

    def __init__(self, pid, pname, sid, sname, cid, cname, unum, uprice, snum, onum, ronum, isabord):
        """初始化函数, 声明所有产品类型的属性"""
        self.productid = pid
        self.productname = pname
        self.supplierid = sid
        self.suppliername = sname
        self.categoryid = cid
        self.categoryname = cname
        self.unitnum = unum
        self.unitprice = uprice
        self.stocknum = snum
        self.ordernum = onum
        self.reordernum = ronum
        self.isabord = isabord


class ProductEncoder(json.JSONEncoder):
    """产品编码类, 主要用于 json 反序列化对象列表时调用"""

    def default(self, obj):
        """用于定义反序列化方式，在 json.dumps(items, clas=ProductEncoder)方法中使用"""
        if isinstance(obj, Product):
            return obj.__dict__
        return json.JSONEncoder.default(self, obj)


class Supplier(object):
    """供应商类"""

    def __init__(self, sid, sname):
        """初始化函数, 定义了供应商编号和供应商名称"""
        self.supplierid = sid
        self.suppliername = sname


class Category(object):
    """产品类别类型"""

    def __init__(self, cid, cname):
        """初始化函数, 定义了类别编号和类别名称"""
        self.categoryid = cid
        self.categoryname = cname
