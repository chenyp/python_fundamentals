# 通用模块

> 兴趣驱动学习，让学习成为一种习惯！
>
> 达内直播课 TEDU LIVE VIDEO
>
> 订阅号: TLV_CN
>
> 服务号: TEDU_TMOOC

---

<!-- TOC -->

- [1. northwind 模块](#1-northwind-模块)
    - [1.1. northwind.json 文件](#11-northwindjson-文件)
    - [1.2. northwind.model 模块](#12-northwindmodel-模块)
    - [1.3. northwind_helper.py文件](#13-northwind_helperpy文件)

<!-- /TOC -->

---

<a id="markdown-1-northwind-模块" name="1-northwind-模块"></a>
## 1. northwind 模块

northwind 模块用于获取 79 种json 格式的产品数据, 并通过集合的方式返回

数据来自微软 Northwind 数据库库(Access)

<a id="markdown-11-northwindjson-文件" name="11-northwindjson-文件"></a>
### 1.1. northwind.json 文件

使用 json 格式存储 79 种商品信息, 部分代码格式如下所示:

```json
[
    {
        "ProductId": 1,
        "ProductName": "苹果汁",
        "SupplierId": 1,
        "SupplierName": "佳佳乐",
        "CategoryId": 1,
        "CategoryName": "饮料",
        "UnitNum": "每箱24瓶",
        "UnitPrice": 18,
        "StockNum": 22,
        "OrderNum": 0,
        "ReOrderNum": 10,
        "IsAbord": false
    },
    {
        "ProductId": 2,
        "ProductName": "牛奶",
        "SupplierId": 1,
        "SupplierName": "佳佳乐",
        "CategoryId": 1,
        "CategoryName": "饮料",
        "UnitNum": "每箱24瓶",
        "UnitPrice": 19,
        "StockNum": 17,
        "OrderNum": 40,
        "ReOrderNum": 25,
        "IsAbord": false
    }
]
```

<a id="markdown-12-northwindmodel-模块" name="12-northwindmodel-模块"></a>
### 1.2. northwind.model 模块

定义了产品类(Product), 供应商类(Supplier), 类别类(Category)

这3个类型用于获取 json 文件中的产品数据, 并封装成类, 代码如下所示

```python
class Product(object):
    """产品类, 根据 json 数据定义产品类型"""

    def __init__(self, pid, pname, sid, sname, cid, cname, unum, uprice, snum, onum, ronum, isabord):
        """初始化函数, 声明所有产品类型的属性"""
        self.productid = pid
        self.productname = pname
        self.supplierid = sid
        self.suppliername = sname
        self.categoryid = cid
        self.categoryname = cname
        self.unitnum = unum
        self.unitprice = uprice
        self.stocknum = snum
        self.ordernum = onum
        self.reordernum = ronum
        self.isabord = isabord
```

```python
class Supplier(object):
    """供应商类"""

    def __init__(self, sid, sname):
        """初始化函数, 定义了供应商编号和供应商名称"""
        self.supplierid = sid
        self.suppliername = sname
```

```python
class Category(object):
    """产品类别类型"""

    def __init__(self, cid, cname):
        """初始化函数, 定义了类别编号和类别名称"""
        self.categoryid = cid
        self.categoryname = cname
```

如果需要将列表反序列化成 json 文件, 需要自定义编码类, 例如

```python
class ProductEncoder(json.JSONEncoder):
    """产品编码类, 主要用于 json 反序列化对象列表时调用"""

    def default(self, obj):
        """用于定义反序列化方式，在 json.dumps(items, clas=ProductEncoder)方法中使用"""
        if isinstance(obj, Product):
            return obj.__dict__
        return json.JSONEncoder.default(self, obj)
```

<a id="markdown-13-northwind_helperpy文件" name="13-northwind_helperpy文件"></a>
### 1.3. northwind_helper.py文件

定义了读写 json 文件的方法, 同时将 json 格式的产品信息转换成列表

[详细信息](northwind/northwind_helper.py)