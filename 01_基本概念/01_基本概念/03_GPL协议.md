# 03_GPL协议

> 兴趣驱动学习，让学习成为一种习惯！
>
> 达内直播课 TEDU LIVE VIDEO
>
> 订阅号: TLV_CN
>
> 服务号: TEDU_TMOOC

---

<!-- TOC -->

- [1. GNU 通用公共许可协议](#1-gnu-通用公共许可协议)
- [2. Copyleft](#2-copyleft)

<!-- /TOC -->

---

<a id="markdown-1-gnu-通用公共许可协议" name="1-gnu-通用公共许可协议"></a>
## 1. GNU 通用公共许可协议

[基维百科](https://zh.wikipedia.org/wiki/GNU通用公共许可证)

英文是：**GNU General Public License**, GPL。

这是一个是广泛使用的免费软件许可证，

可以保证终端用户得自由运行，学习，共享和修改软件。

许可证由GNU项目的自由软件基金会的 理查德·斯托曼
[基维百科](https://zh.wikipedia.org/wiki/理查德·斯托曼) 撰写，并授予计算机程序的收件人自由软件定义的权利。

<a id="markdown-2-copyleft" name="2-copyleft"></a>
## 2. Copyleft

GPL 是一个 Copyleft 许可证，这意味着派生作品只能以相同的许可条款分发。

- GPL 授予程序接受人以下权利，或称自由，或称 copyleft
- 以任何目的运行此程序的自由
- 再复制的自由
- 改进此程序，并公开发布改进的自由（前提是能得到源代码）