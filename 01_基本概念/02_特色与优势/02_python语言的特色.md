# 02_python语言的特色

> 兴趣驱动学习，让学习成为一种习惯！
>
> 达内直播课 TEDU LIVE VIDEO
>
> 订阅号: TLV_CN
>
> 服务号: TEDU_TMOOC

---

<!-- TOC -->

- [1. 可移植](#1-可移植)
- [2. 可嵌入](#2-可嵌入)
- [3. 可扩展](#3-可扩展)

<!-- /TOC -->

---

<a id="markdown-1-可移植" name="1-可移植"></a>
## 1. 可移植

Python 是一种可以跨操作平台运行的计算机程序设计语言

即 Python 程序的核心语言和标准库可以在 Linux、Windows，及其他带有 [Python解释器](http://blog.51cto.com/12306609/2102983) 的平台上无差别的运行。

其原因有如下3个方面：

1. Python 发行时自带的标准库和模块在实现上尽可能的考虑到了跨平台的可移植性
2. Python 程序可以自动编译成可移植的字节码
3. Python 的标准实现是由可移植的ANSI C [基维百科](https://baike.baidu.com/item/ANSI%20C/7657277) 编写的

<a id="markdown-2-可嵌入" name="2-可嵌入"></a>
## 2. 可嵌入

[百度百科](https://baike.baidu.com/item/Python/407313?fr=aladdin)

就是可以把 Python 语言嵌入 C/C++ 程序中，从而向程序用户提供脚本功能。

<a id="markdown-3-可扩展" name="3-可扩展"></a>
## 3. 可扩展

参考资料：[百度百科](https://baike.baidu.com/item/Python/407313?fr=aladdin#9)

如果你想让一段关键代码运行得更快或者希望某些算法不公开，

就可以把你的部分程序用 C 或 C++ 编写，然后在 Python 程序中使用它们。
