#!/usr/bin/env python
# -*- coding:UTF-8 -*-
"""
liu yi
2018/4/28
ex003_while循环计算1-100累加的和.py
"""

# 定义sum用于存储计算结果
sum = 0
n = 1
# 下面的循环会改变n的值从1—100
while n <= 100:
    # 每生成一个数就将它加到sum中
    sum += n
    n += 1
print(sum)