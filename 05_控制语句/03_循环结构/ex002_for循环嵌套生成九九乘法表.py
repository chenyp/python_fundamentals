#!/usr/bin/env python
# -*- coding:UTF-8 -*-
"""
liu yi
2018/4/27
ex002_for循环嵌套生成九九乘法表.py
"""

# 此循环控制行数，1-9
for i in range(1, 10):
    # 该控制列数
    for k in range(1, i + 1):
        print("%sX%s=%s" % (k, i, k * i), end='  ')
    print()
