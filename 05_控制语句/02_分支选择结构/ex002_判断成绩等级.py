#!/usr/bin/python3
# -*-coding:UTF-8 -*-
"""
liuyi
2018/4/27
ex002_判断成绩等级.py

说明：使用if else 判断学生成绩等级
"""

# 定义一个score变量接收用户输入并转化为整型
score = int(input("请输入学生成绩："))

# 判断用户输入成绩是否大于等于90
if score >= 90:
    print("A")
# 如果上面的判断不成立，则判断是否大于等于80
elif score >= 80:
    print("B")
# 如果前面的不成立，则判断是否大于等于60
elif score >= 60:
    print("C")
# 如果以上全部不成立则直接运行else
else:
    print("不及格")
