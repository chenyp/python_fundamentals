# 02_MacOS系统

> 兴趣驱动学习，让学习成为一种习惯！
>
> 达内直播课 TEDU LIVE VIDEO
>
> 订阅号: TLV_CN
>
> 服务号: TEDU_TMOOC

---

<!-- TOC -->

- [1. MacOS版安装](#1-macos版安装)
- [2. 相关视频](#2-相关视频)

<!-- /TOC -->

---

<a id="markdown-1-macos版安装" name="1-macos版安装"></a>
## 1. MacOS版安装

MacOS系统一般默认安装了python2.7，需要我们手动下载安装python3

进入 [python官网下载](https://www.python.org/downloads/mac-osx/)

选择MacOS系统的python版本

![版本选择](https://gitee.com/liuyicz/picture/raw/master/macos%E7%B3%BB%E7%BB%9Fpython%E7%89%88%E6%9C%AC%E9%80%89%E6%8B%A9.png)

下载完毕后双击安装包，安装完成即可

安装完成后打开终端输入python3回车，出现以下界面即安装成功

![测试安装](https://gitee.com/liuyicz/picture/raw/master/MacOS%E6%B5%8B%E8%AF%95.png)

<a id="markdown-2-相关视频" name="2-相关视频"></a>
## 2. 相关视频