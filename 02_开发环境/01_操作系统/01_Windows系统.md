# 01_Windows系统

> 兴趣驱动学习，让学习成为一种习惯！
>
> 达内直播课 TEDU LIVE VIDEO
>
> 订阅号: TLV_CN
>
> 服务号: TEDU_TMOOC

---

<!-- TOC -->

- [1. Python安装](#1-python安装)
    - [1.1. 下载](#11-下载)
    - [1.2. 相关视频](#12-相关视频)
        - [1.2.1. python下载安装](#121-python下载安装)
        - [1.2.2. pycharm的使用](#122-pycharm的使用)

<!-- /TOC -->

---

<a id="markdown-1-python安装" name="1-python安装"></a>
## 1. Python安装

<a id="markdown-11-下载" name="11-下载"></a>
### 1.1. 下载

进入 [python官网](https://www.python.org/downloads/) 选择版本

![选择安装程序](https://gitee.com/liuyicz/picture/raw/master/%E5%AE%89%E8%A3%85%E5%8C%85%E9%80%89%E6%8B%A9.png)

下载完毕后双击安装包，自动添加环境变量要勾选上，然后单击 `install now`

![安装](https://gitee.com/liuyicz/picture/raw/master/python%E5%AE%89%E8%A3%85.png)

完成后测试打开终端输入 `python` 如下图即安装成功

![测试](https://gitee.com/liuyicz/picture/raw/master/%E6%B5%8B%E8%AF%95.png)

<a id="markdown-12-相关视频" name="12-相关视频"></a>
### 1.2. 相关视频

<a id="markdown-121-python下载安装" name="121-python下载安装"></a>
#### 1.2.1. python下载安装

[爱奇艺](http://www.iqiyi.com/w_19ry0lvlh5.html#vfrm=8-8-0-1)

<a id="markdown-122-pycharm的使用" name="122-pycharm的使用"></a>
#### 1.2.2. pycharm的使用

[爱奇艺](http://www.iqiyi.com/w_19ry0kr1m1.html#vfrm=8-8-0-1)